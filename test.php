<div class="row col s12 center">
	<h3 class="white-text typ-orbitron center"><i class="material-icons medium">store</i>BLUEFIRE SHOP</h3>
</div>

<div class="item-list-cont container">
	<div class="row center">

		<div class="item-block-cont col s12 m6 l4">
			<div class="item-block">
				<div class="item-img-cont">
					<img class="responsive-img" src="common/img/products/acerpredator.png">
				</div>
				<div class="item-desc-cont">
					<h5 class="item-name center typ-orbitron">Predator Helios 300</h5>
					<hr>
					<strong class="blue-text">Brand:&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</strong><span class="item-brand">Acer</span>
					<strong class="blue-text">&nbsp&nbsp&nbsp&nbsp&nbsp&nbspCategory: </strong><span class="item-categoy">Laptop</span>
					<hr>
					<strong class="blue-text">Price (₱hp):&nbsp&nbsp&nbsp</strong><span class="item-price">55000</span>
					<hr>
					<strong class="blue-text">Description: </strong><hr>
					<div class="item-desc">Conquer the competition with the latest 7th Gen Intel® Core™ i7 processor1 and overclockable. Conquer the competition with the latest 7th Gen Intel® Core™ i7 processor1 and overclockable. Conquer the competition with the latest 7th Gen Intel® Core™ i7 processor1 and overclockable. Conquer the competition with the latest 7th Gen Intel® Core™ i7 processor1 and overclockable. </div>
				</div>
				<button class="add-cart-btn waves-effect waves-teal btn blue accent-4"><i class="material-icons left">add_shopping_cart</i> ADD TO CART</button>
				<button data-tooltip="Delete this item" class="tooltipped del-item btn-floating waves-effect waves-light red darken-4"><i class="material-icons left">clear</i></button>
				<button data-tooltip="Edit this item" class="tooltipped edit-item btn-floating waves-effect waves-light light-blue accent-3"><i class="material-icons left">edit</i></button>
				<div class="item-block-decor">----BLUEFIRE--</div>
			</div>
		</div>

		<div class="item-block-cont col s12 m6 l4">
			<div class="item-block">
				<div class="item-img-cont">
					<img class="responsive-img" src="common/img/products/acerpredator.png">
				</div>
				<div class="item-desc-cont">
					<h5 class="item-name center typ-orbitron">Predator Helios 300</h5>
					<hr>
					<strong class="blue-text">Brand:&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</strong><span class="item-brand">Acer</span>
					<strong class="blue-text">&nbsp&nbsp&nbsp&nbsp&nbsp&nbspCategory: </strong><span class="item-categoy">Laptop</span>
					<hr>
					<strong class="blue-text">Price (₱hp):&nbsp&nbsp&nbsp</strong><span class="item-price">55000</span>
					<hr>
					<strong class="blue-text">Description: </strong><hr>
					<div class="item-desc">Conquer the competition with the latest 7th Gen Intel® Core™ i7 processor1 and overclockable. Conquer the competition with the latest 7th Gen Intel® Core™ i7 processor1 and overclockable. Conquer the competition with the latest 7th Gen Intel® Core™ i7 processor1 and overclockable. Conquer the competition with the latest 7th Gen Intel® Core™ i7 processor1 and overclockable. </div>
				</div>
				<button class="add-cart-btn waves-effect waves-teal btn blue accent-4"><i class="material-icons left">add_shopping_cart</i> ADD TO CART</button>
				<button data-tooltip="Delete this item" class="tooltipped del-item btn-floating waves-effect waves-light red darken-4"><i class="material-icons left">clear</i></button>
				<button data-tooltip="Edit this item" class="tooltipped edit-item btn-floating waves-effect waves-light light-blue accent-3"><i class="material-icons left">edit</i></button>
				<div class="item-block-decor">----BLUEFIRE--</div>
			</div>
		</div>

		<div class="item-block-cont col s12 m6 l4">
			<div class="item-block">
				<div class="item-img-cont">
					<img class="responsive-img" src="common/img/products/acerpredator.png">
				</div>
				<div class="item-desc-cont">
					<h5 class="item-name center typ-orbitron">Predator Helios 300</h5>
					<hr>
					<strong class="blue-text">Brand:&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</strong><span class="item-brand">Acer</span>
					<strong class="blue-text">&nbsp&nbsp&nbsp&nbsp&nbsp&nbspCategory: </strong><span class="item-categoy">Laptop</span>
					<hr>
					<strong class="blue-text">Price (₱hp):&nbsp&nbsp&nbsp</strong><span class="item-price">55000</span>
					<hr>
					<strong class="blue-text">Description: </strong><hr>
					<div class="item-desc">Conquer the competition with the latest 7th Gen Intel® Core™ i7 processor1 and overclockable. Conquer the competition with the latest 7th Gen Intel® Core™ i7 processor1 and overclockable. Conquer the competition with the latest 7th Gen Intel® Core™ i7 processor1 and overclockable. Conquer the competition with the latest 7th Gen Intel® Core™ i7 processor1 and overclockable. </div>
				</div>
				<button class="add-cart-btn waves-effect waves-teal btn blue accent-4"><i class="material-icons left">add_shopping_cart</i> ADD TO CART</button>
				<button data-tooltip="Delete this item" class="tooltipped del-item btn-floating waves-effect waves-light red darken-4"><i class="material-icons left">clear</i></button>
				<button data-tooltip="Edit this item" class="tooltipped edit-item btn-floating waves-effect waves-light light-blue accent-3"><i class="material-icons left">edit</i></button>
				<div class="item-block-decor">----BLUEFIRE--</div>
			</div>
		</div>

		<div class="item-block-cont col s12 m6 l4">
			<div class="item-block">
				<div class="item-img-cont">
					<img class="responsive-img" src="common/img/products/acerpredator.png">
				</div>
				<div class="item-desc-cont">
					<h5 class="item-name center typ-orbitron">Predator Helios 300</h5>
					<hr>
					<strong class="blue-text">Brand:&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</strong><span class="item-brand">Acer</span>
					<strong class="blue-text">&nbsp&nbsp&nbsp&nbsp&nbsp&nbspCategory: </strong><span class="item-categoy">Laptop</span>
					<hr>
					<strong class="blue-text">Price (₱hp):&nbsp&nbsp&nbsp</strong><span class="item-price">55000</span>
					<hr>
					<strong class="blue-text">Description: </strong><hr>
					<div class="item-desc">Conquer the competition with the latest 7th Gen Intel® Core™ i7 processor1 and overclockable. Conquer the competition with the latest 7th Gen Intel® Core™ i7 processor1 and overclockable. Conquer the competition with the latest 7th Gen Intel® Core™ i7 processor1 and overclockable. Conquer the competition with the latest 7th Gen Intel® Core™ i7 processor1 and overclockable. </div>
				</div>
				<button class="add-cart-btn waves-effect waves-teal btn blue accent-4"><i class="material-icons left">add_shopping_cart</i> ADD TO CART</button>
				<button data-tooltip="Delete this item" class="tooltipped del-item btn-floating waves-effect waves-light red darken-4"><i class="material-icons left">clear</i></button>
				<button data-tooltip="Edit this item" class="tooltipped edit-item btn-floating waves-effect waves-light light-blue accent-3"><i class="material-icons left">edit</i></button>
				<div class="item-block-decor">----BLUEFIRE--</div>
			</div>
		</div>

		<div class="item-block-cont col s12 m6 l4">
			<div class="item-block">
				<div class="item-img-cont">
					<img class="responsive-img" src="common/img/products/acerpredator.png">
				</div>
				<div class="item-desc-cont">
					<h5 class="item-name center typ-orbitron">Predator Helios 300</h5>
					<hr>
					<strong class="blue-text">Brand:&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</strong><span class="item-brand">Acer</span>
					<strong class="blue-text">&nbsp&nbsp&nbsp&nbsp&nbsp&nbspCategory: </strong><span class="item-categoy">Laptop</span>
					<hr>
					<strong class="blue-text">Price (₱hp):&nbsp&nbsp&nbsp</strong><span class="item-price">55000</span>
					<hr>
					<strong class="blue-text">Description: </strong><hr>
					<div class="item-desc">Conquer the competition with the latest 7th Gen Intel® Core™ i7 processor1 and overclockable. Conquer the competition with the latest 7th Gen Intel® Core™ i7 processor1 and overclockable. Conquer the competition with the latest 7th Gen Intel® Core™ i7 processor1 and overclockable. Conquer the competition with the latest 7th Gen Intel® Core™ i7 processor1 and overclockable. </div>
				</div>
				<button class="add-cart-btn waves-effect waves-teal btn blue accent-4"><i class="material-icons left">add_shopping_cart</i> ADD TO CART</button>
				<button data-tooltip="Delete this item" class="tooltipped del-item btn-floating waves-effect waves-light red darken-4"><i class="material-icons left">clear</i></button>
				<button data-tooltip="Edit this item" class="tooltipped edit-item btn-floating waves-effect waves-light light-blue accent-3"><i class="material-icons left">edit</i></button>
				<div class="item-block-decor">----BLUEFIRE--</div>
			</div>
		</div>

		<div class="item-block-cont col s12 m6 l4">
			<div class="item-block">
				<div class="item-img-cont">
					<img class="responsive-img" src="common/img/products/acerpredator.png">
				</div>
				<div class="item-desc-cont">
					<h5 class="item-name center typ-orbitron">Predator Helios 300</h5>
					<hr>
					<strong class="blue-text">Brand:&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</strong><span class="item-brand">Acer</span>
					<strong class="blue-text">&nbsp&nbsp&nbsp&nbsp&nbsp&nbspCategory: </strong><span class="item-categoy">Laptop</span>
					<hr>
					<strong class="blue-text">Price (₱hp):&nbsp&nbsp&nbsp</strong><span class="item-price">55000</span>
					<hr>
					<strong class="blue-text">Description: </strong><hr>
					<div class="item-desc">Conquer the competition with the latest 7th Gen Intel® Core™ i7 processor1 and overclockable. Conquer the competition with the latest 7th Gen Intel® Core™ i7 processor1 and overclockable. Conquer the competition with the latest 7th Gen Intel® Core™ i7 processor1 and overclockable. Conquer the competition with the latest 7th Gen Intel® Core™ i7 processor1 and overclockable. </div>
				</div>
				<button class="add-cart-btn waves-effect waves-teal btn blue accent-4"><i class="material-icons left">add_shopping_cart</i> ADD TO CART</button>
				<button data-tooltip="Delete this item" class="tooltipped del-item btn-floating waves-effect waves-light red darken-4"><i class="material-icons left">clear</i></button>
				<button data-tooltip="Edit this item" class="tooltipped edit-item btn-floating waves-effect waves-light light-blue accent-3"><i class="material-icons left">edit</i></button>
				<div class="item-block-decor">----BLUEFIRE--</div>
			</div>
		</div>


	</div>
</div>