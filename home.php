<?php

function display_title() {
	echo "BlueFire | Capstone 2 | Buy Gaming Laptops & Accessories Online";
}

require "template.php";

function display_content() {

	echo isset($_SESSION['userID']) ?
	"<h6 class='flow-text center typ-orbitron white-text hide-on-large-only white-border mobile-greet'>HELLO " . strtoupper($_SESSION['userID']) . "!</h6>" : "";

	echo "<h4 class='flow-text center typ-orbitron white-text'>YOU CAN STAY <strong class='blue-text'>COOL</strong> IN THE <strong class='red-text'>HEAT</strong> OF THE BATTLE.<br>OUR RIGS ARE ENGINEERED TO <strong class='orange-text'>DOMINATE.</strong></h4>";
	echo "<!-- HOME PAGE CAROUSEL SECTION -->";
	echo "<div class='hero-carousel carousel carousel-slider center' data-indicators='true'>";
	echo "<a class='carousel-item' href='#one!'><img src='common/img/campaign/sennheiser.jpg'></a>";
	echo "<a class='carousel-item' href='#two!'><img src='common/img/campaign/razer-valerie.gif'></a>";
	echo "<a class='carousel-item' href='#three!'><img src='common/img/campaign/razer-ouroboros.png'></a>";
	echo "<a class='carousel-item' href='#four!'><img src='common/img/campaign/predator-1.jpg'></a>";
	echo "<a class='carousel-item' href='#five!'><img src='common/img/campaign/oculus.jpg'></a>";
	echo "<a class='carousel-item' href='#six!'><img src='common/img/campaign/logitech.jpg'></a>";
	echo "</div>";
	echo "<div class='hero-button-cont center'>";
	echo "<a href='shop.php' class='btn hero-button waves-bluefire white-text pulse m-bottom-30'>SHOP BLUEFIRE!</a>";
	echo "<div class='white-border m-top-30'>";
	echo "<h5 class='typ-orbitron blue-text'>LET US EQUIP YOU</h5>";
	echo "<hr>";
	echo "<div class='row'>";
	echo "<p class='white-text typ-nunito col s12 m6 l6'>Shopping for computer gear that's right for your gaming needs shouldn't be difficult. If you work hard and play hard, or if you simply want to upgrade and refresh your gaming gear, you have come to the right place, because <strong class='blue-text'>BlueFire</strong> will help you get off the ground and conquer even the most-demanding games with ease with our powerful hardware. Popular brands like ASUS, Razer, Toshiba, Dell, Lenovo and MSI offer the power you need to dominate the playing field. Take a look and see our wide range of options.</p>";
	echo "<p class='white-text typ-nunito col s12 m6 l6'>Most of our hardware come with an extended array of features inluding HD displays and Superb Sound Systems. BlueFire offers you a large variety of laptops to choose from. BlueFire is a one-stop shop for you to find the best and only the most powerful gaming hardware available in the Philippines. Portable, powerful and wide variety of laptop computers from the best brands are at your disposal. Take a tour of our shop and browse our products at your pleasure and own pace.</p>";
	echo "</div>";
	echo "</div>";

	// BEGIN FEATURED ITEMS
	echo "<div class='row col s12 center' style='margin-top: 60px; margin-bottom: 0px'>";
	echo "<h3 class='white-text typ-orbitron center'>FEATURED ITEMS</h3>";
	echo "</div>";
	echo "<div class='item-list-cont container'>";
	echo "<div class='row center'>";
	echo "<!-- FEATURED ITEM 1 -->";
	echo "<div class='item-block-cont col s12 m12 l4'>";
	echo "<div class='item-block'>";
	echo "<div class='item-img-cont'>";
	echo "<img src='common/img/campaign/acerpredator.png'>";
	echo "</div>";
	echo "<div class='item-desc-cont'>";
	echo "<h5 class='center typ-orbitron'>Predator Helios 300</h5>";
	echo "<hr>";
	echo "<strong class='blue-text'>Brand:&nbsp&nbsp&nbsp</strong><span class='item-brand'>Acer</span>";
	echo "<strong class='blue-text'>&nbsp&nbsp&nbspCategory:&nbsp&nbsp&nbsp</strong><span class='item-category'> Laptop</span>";
	echo "<hr>";
	echo "<strong class='blue-text'>Price (₱hp):&nbsp&nbsp&nbsp</strong><span class='item-price'>55000</span>";
	echo "<hr>";
	echo "<strong class='blue-text'>Description: </strong><hr>";
	echo "<div class='item-desc'>The Acer Predator Helios 300 offers a powerful, VR-ready Nvidia GeForce GTX 1060 GPU for a lower price than competitors and runs circles around the GTX 1050 Ti cards that competing laptops use at that price.</div>";
	echo "</div>";
	echo "<button class='shop_link add-cart-btn waves-effect waves-bluefire btn blue accent-4'><i class='material-icons left'>store</i> GO TO SHOP</button>";
	echo "<div class='item-block-decor'>----FEATURED--</div>";
	echo "</div>";
	echo "</div>";
	echo "<!-- FEATURED ITEM 2 -->";
	echo "<div class='item-block-cont col s12 m12 l4'>";
	echo "<div class='item-block'>";
	echo "<div class='item-img-cont'>";
	echo "<img src='common/img/campaign/sennheiser-gsp-300.png'>";
	echo "</div>";
	echo "<div class='item-desc-cont'>";
	echo "<h5 class='center typ-orbitron'>GSP-300 Headset</h5>";
	echo "<hr>";
	echo "<strong class='blue-text'>Brand:&nbsp&nbsp&nbsp</strong><span class='item-brand'>Sennheiser</span>";
	echo "<strong class='blue-text'>&nbsp&nbsp&nbspCategory: &nbsp</strong><span class='item-category'> Headset</span>";
	echo "<hr>";
	echo "<strong class='blue-text'>Price (₱hp):&nbsp&nbsp&nbsp</strong><span class='item-price'>4999</span>";
	echo "<hr>";
	echo "<strong class='blue-text'>Description: </strong><hr>";
	echo "<div class='item-desc'>The GSP 300 is Sennheiser’s headset for gamers who want a versatile product that’s suitable for music, gaming and chat. It combines a highly respectable build with solid audio quality, but remains lightweight and comfortable over long periods of time.</div>";
	echo "</div>";
	echo "<button class='shop_link add-cart-btn waves-effect waves-bluefire btn blue accent-4'><i class='material-icons left'>store</i> GO TO SHOP</button>";
	echo "<div class='item-block-decor'>----FEATURED--</div>";
	echo "</div>";
	echo "</div>";
	echo "<!-- FEATURED ITEM 3 -->";
	echo "<div class='item-block-cont col s12 m12 l4'>";
	echo "<div class='item-block'>";
	echo "<div class='item-img-cont'>";
	echo "<img src='common/img/campaign/razer-ouroboros2.png'>";
	echo "</div>";
	echo "<div class='item-desc-cont'>";
	echo "<h5 class='center typ-orbitron'>Ouroboros</h5>";
	echo "<hr>";
	echo "<strong class='blue-text'>Brand:&nbsp&nbsp&nbsp</strong><span class='item-brand'>Razer</span>";
	echo "<strong class='blue-text'>&nbsp&nbsp&nbspCategory:&nbsp&nbsp&nbsp</strong><span class='item-category'> Mouse</span>";
	echo "<hr>";
	echo "<strong class='blue-text'>Price (₱hp):&nbsp&nbsp&nbsp</strong><span class='item-price'>7700</span>";
	echo "<hr>";
	echo "<strong class='blue-text'>Description: </strong><hr>";
	echo "<div class='item-desc'>This beast of a mouse is outfitted with the most advanced and configurable sensor yet, the all-new 8200dpi 4G laser sensor. It tracks so precisely, you always hit targets exactly where you need to. It can also be calibrated to your specific surface, and has adjustable cut-off to lift-off tracking, empowering you with more control than ever.</div>";
	echo "</div>";
	echo "<button class='shop_link add-cart-btn waves-effect waves-bluefire btn blue accent-4'><i class='material-icons left'>store</i> GO TO SHOP</button>";
	echo "<div class='item-block-decor'>----FEATURED--</div>";
	echo "</div>";
	echo "</div>";
	echo "</div>";
	echo "</div>";
	// END FEATURED ITEMS

	echo "<!-- HOME PAGE PARALLAX SECTION -->";
	echo "<!-- PARALLAX 1 -->";
	echo "<div class='parallax-container m-top-60'>";
	echo "<div class='parallax' id='parallax_img1'>";
	echo "<div data-aos='fade-left' data-aos-duration='3000' data-aos-delay='450' class='parallax_caption1' style='left: 10px;'>No matter what kind of WORLDS you DWELL,</div>";
	echo "<img style='bottom: -330px' src='common/img/campaign/skyrim.jpg'>";
	echo "</div>";
	echo "</div>";
	echo "<!-- PARALLAX 2 -->";
	echo "<div class='parallax-container' style='margin-top: 0px;'>";
	echo "<div class='parallax' id='parallax_img2'>";
	echo "<div data-aos='fade-right' data-aos-duration='3000' data-aos-delay='450' class='parallax_caption1' style='right: 10px;'>the sort of HEROES or VILLAINS you PLAY,</div>";
	echo "<img src='common/img/campaign/dota2.jpg'>";
	echo "</div>";
	echo "</div>";
	echo "<!-- PARALLAX 3 -->";
	echo "<div class='parallax-container' style='margin-top: 0px;'>";
	echo "<div class='parallax' id='parallax_img3'>";
	echo "<div data-aos='fade-left' data-aos-duration='3000' data-aos-delay='450' class='parallax_caption1' style='left: 10px;'>nor the type of WEAPONS you CHOOSE.</div>";
	echo "<img src='common/img/campaign/csgo.jpg'>";
	echo "</div>";
	echo "</div>";
	echo "<!-- PARALLAX 4 -->";
	echo "<div class='parallax-container' style='margin-top: 0px;'>";
	echo "<div class='parallax' id='parallax_img4'>";
	echo "<div data-aos='fade-right' data-aos-duration='3000' data-aos-delay='450' class='parallax_caption1' style='right: 10px;'>BLUEFIRE has always got your BACK.</div>";
	echo "<img src='common/img/campaign/champzed.jpg'>";
	echo "</div>";
	echo "</div>";
	echo "<!-- PARALLAX 5 AND SECOND SHOP BUTTON -->";
	echo "<div class='parallax-container' style='margin-top: 0px; height: 500px;'>";
	echo "<h4 class='white-text typ-orbitron'>- <strong class='red-text'>TURN ON </strong>THE<strong class='red-text'> HEAT</strong> -</h4>";
	echo "<div class='parallax' id='parallax_img5'>";
	echo "<div data-aos='fade-up' data-aos-duration='3000' data-aos-delay='450' class='parallax_caption1 caption1_b'><small style='text-shadow: none;' class='red-text'>EXLUSIVE OFFERS AWAIT</small>";
	echo "<div class='shop_link hero-button-cont center'><a style='text-shadow: none; background-color: #ff00001a;' href='#!' class='btn hero-button waves-bluefire white-text pulse'>GET YOUR GEAR NOW!</a></div>";
	echo "</div>";
	echo "<img class='dennis-custom-glow1' src='common/img/campaign/champzed.jpg'>";
	echo "</div>";
	echo "</div>";
	echo "<div class='white-border m-top-30'>";
	echo "<h5 class='typ-orbitron red-text'>GUARANTEED TO LAST</h5>";
	echo "<hr>";
	echo "<div class='row'>";
	echo "<p class='white-text typ-nunito col s12'>Turn the tides of battle with our hardware and you'll swear by the sheer force our gears has to offer. Our gaming laptops are designed with advanced architecture and designed to last. Never be obsolete with our upgrade services and warranties. Choose from our products and select the best after-sales package to suit your needs.</p>";
	echo "</div>";
	echo "</div>";
}






?>