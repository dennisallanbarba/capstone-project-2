<?php
require 'connection.php';

if(isset($_POST['userID'])){
	$userID = $_POST['userID'];
	$userPassword = sha1($_POST['userPassword']);

	$sql = "SELECT * FROM users WHERE username = '$userID' AND password = '$userPassword'";
	$result = mysqli_query($conn,$sql);

	if(mysqli_num_rows($result)>0){
		$row = mysqli_fetch_assoc($result);
		session_start();
		$_SESSION['userID'] = $userID;
		$_SESSION['role'] = $row['role'];
		header('location: home.php');
	} else {
		require "template.php";
	}

}

function display_title() {
	echo "BlueFire - Login";
}

function display_content() {
	echo "<div class='row'>";
	echo "<div class='col s1 m2 l2'></div>";
	echo "<div class='col s10 m8 l8'>";
	echo "<div class='card horizontal grey darken-4 white-text'>";
	echo "<div class='card-stacked center'>";
	echo "<div class='card-content'>";
	echo "<h5>Invalid user ID or password.<br><br>Please try <a class='modal-trigger' href='#modal_login'>logging in</a> again.</h5>";
	echo "</div>";
	echo "</div>";
	echo "</div>";
	echo "</div>";
	echo "<div class='col s1 m2 l2'></div>";
	echo "</div>";
}