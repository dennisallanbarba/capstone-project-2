<?php

header('refresh: 10; url=home.php');

require "template.php";
session_destroy();

function display_title() {
	echo "BlueFire - Logout";
}

function display_content() {
echo "<div class='row'>";
echo "<div class='col s1 m2 l2'></div>";
echo "<div class='col s10 m8 l8'>";
echo "<div class='card horizontal grey darken-4 white-text'>";
echo "<div class='card-image'>";
echo "<img style='height: 100%;' src='common/img/bluefire-logout.jpg'>";
echo "</div>";
echo "<div class='card-stacked center'>";
echo "<div class='card-content'>";
echo "<h5>You have successfully logged out.</h5>";
echo "</div>";
echo "<div class='card-action'>";
echo "<p>If you are not automatically redirected in <span class='cdtimer'>10</span> seconds click <a href='home.php'>HERE</a></p>";
echo "</div>";
echo "</div>";
echo "</div>";
echo "</div>";
echo "<div class='col s1 m2 l2'></div>";
echo "</div>";
}

?>
