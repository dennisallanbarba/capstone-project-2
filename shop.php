<?php
	require 'template.php';
	// session_start();
	function display_title() {
		echo "BlueFire | Shop";
	}

function display_content() {
require 'connection.php';
echo "<div class='row col s12 center'>";
echo "<h3 class='white-text typ-orbitron center'><i class='material-icons medium'>store</i>BLUEFIRE SHOP</h3>";
echo "</div>";
echo "<div class='item-list-cont container'>";
echo "<div class='row center'>";

// ADD ITEMS BUTTON
if(isset($_SESSION['userID']) && $_SESSION['role']=='admin') {
	echo "<button href='#modal_add_item' id='add_item_button' class='modal-trigger btn-floating green accent-3 pulsehover tooltipped' data-tooltip='Add a new product' data-position='right'><i class='material-icons'>add_to_photos</i></button>";

	// ADD ITEMS MODAL
	echo "<div id='modal_add_item' class='modal grey darken-4'>";
	echo "<div id='add_item_h4' class='row center modal-content col s12 white-text typ-orbitron'  style=''>";
	echo "<h4>ADD NEW PRODUCT</h4>";
	echo "</div>";

	echo "<div class='container add_item_cont'>";
	// BEGIN ADD ITEMS FORM
	echo "<form class='row s12' id='add_item_form' action='add_item.php' method='POST' enctype='multipart/form-data'>";
	echo "<div class='center white-text col s12 m-top-15'>Hi Admin. Add a new product.</div>";

	// PRODUCT NAME productName
	echo "<div class='input-field col s12 center white-text'>";
	echo "<input type='text' id='product_name_add' name='productName' required>";
	echo "<label for='product_name_add'>Product Name</label>";
	echo "</div>";

	// PRODUCT UPLOAD IMAGE productImage
	echo "<div class='input-field col s12 white-text'>";
	echo "<div class='file-field input-field'>";
	echo "<div class='btn blue accent-4'>";
	echo "<i class='material-icons left'>add_to_photos</i>";
	echo "<span>Browse</span>";
	echo "<input name='productImage' type='file' />";
	echo "</div>";
	echo "<div class='file-path-wrapper'>";
	echo "<input class='file-path validate' type = 'text'";
	echo "placeholder='Upload Product Image' />";
	echo "</div>";
	echo "</div>";
	echo "</div>";

	// PRODUCT BRAND productBrand
	echo "<div class='input-field col s12 white-text'>";
	echo "<select name='productBrand'>";
	$sql = "SELECT * FROM brands";
	$result = mysqli_query($conn,$sql);
	echo "<option value='' disabled selected>Select Category</option>";
	while($row = mysqli_fetch_assoc($result)){
		extract($row);
		echo "<option value='$ID'>" . ucfirst($name) . "</option>";
	}
	echo "</select>";
	echo "<label>Brand Name</label>";
	echo "</div>";

	// PRODUCT CATEGORY productCategory
	echo "<div class='input-field col s12 white-text'>";
	echo "<select name='productCategory'>";
	$sql = "SELECT * FROM categories";
	$result = mysqli_query($conn,$sql);
	echo "<option value='' disabled selected>Select Category</option>";
	while($row = mysqli_fetch_assoc($result)){
		extract($row);
		echo "<option value='$id'>" . ucfirst($name) . "</option>";
	}
	echo "</select>";
	echo "<label>Product Category</label>";
	echo "</div>";

	// PRODUCT PRICE productPrice
	echo "<div class='input-field col s12 center white-text'>";
	echo "<input type='number' id='product_price_add' name='productPrice' required>";
	echo "<label for='product_price_add'>Product Price (₱hp)</label>";
	echo "</div>";

	// PRODUCT DESCRIPTION productDesc
	echo "<div class='input-field col s12 white-text'>";
	echo "<textarea id='product_desc_add' class='materialize-textarea' name='productDesc'></textarea>";
	echo "<label for='product_desc_add'>Product Description</label>";
	echo "</div>";

	// ADD PRODUCT SUBMIT BUTTON
	echo "<div class='row col s12'>";
	echo "<button type='submit' value='add_item' class='waves-effect waves-bluefire btn blue accent-4'><i class='material-icons left'>note_add</i>ADD ITEM</button>";
	echo "</div>";
	echo "</form>";
	// END ADD ITEMS FORM
	echo "</div> <!-- END FORM ADD ITEM -->";
	echo "</div> <!-- END MODAL ADD ITEM -->";
	// END ADD ITEMS MODAL

}



// ITERATE ITEMS

$sql = "SELECT *,
products.name AS product_name, 
brands.name AS brand_name,
categories.name AS category_name 
FROM products 
JOIN categories ON products.category_id=categories.id 
JOIN brands ON products.brand_id=brands.id";

$result = mysqli_query($conn,$sql);

while($product = mysqli_fetch_assoc($result)){
	$index = $product['id'];

	echo "<div class='item-block-cont col s12 m6 l4'>";
	echo "<div class='item-block'>";
	echo "<div class='item-img-cont'>";
	/* PRODUCT IMAGE */echo "<img src=" . $product['image'] . ">";
	echo "</div>";
	echo "<div class='item-desc-cont'>";
	/* PRODUCT NAME */echo "<h6 class='item-name center typ-orbitron flow-text'>" . $product['product_name'] . "</h6>";
	echo "<hr>";
	/* PRODUCT BRAND */echo "<strong class='blue-text'>Brand:&nbsp&nbsp&nbsp</strong><span class='item-brand'>" . $product['brand_name'] . "</span>";
	/* PRODUCT CATEGORY */echo "<strong class='blue-text'>&nbsp&nbsp&nbspCategory:&nbsp&nbsp&nbsp</strong><span class='item-categoy'>" . ucfirst($product['category_name']) . "</span>";
	echo "<hr>";
	/* PRODUCT PRICE */echo "<strong class='blue-text'>Price (₱hp):&nbsp&nbsp&nbsp</strong><span class='item-price'>" . $product['price'] . "</span>";
	echo "<hr>";
	echo "<strong class='blue-text'>Description: </strong><hr>";
	/* PRODUCT DESCRIPTION */echo "<div class='item-desc'>" . $product['description'] . "</div>";
	echo "</div>";
	echo "<button class='add-cart-btn waves-effect waves-teal btn blue accent-4'><i class='material-icons left'>add_shopping_cart</i> ADD TO CART</button>";
	if(isset($_SESSION['userID']) && $_SESSION['role']=='admin') {
	echo "<button data-tooltip='Delete this item' class='tooltipped del-item btn-floating waves-effect waves-light red darken-4'><i class='material-icons left'>clear</i></button>";
	echo "<button data-tooltip='Edit this item' class='tooltipped edit-item btn-floating waves-effect waves-light light-blue accent-3'><i class='material-icons left'>edit</i></button>";
	}
	echo "<div class='item-block-decor'>----BLUEFIRE--</div>";
	echo "</div>";
	echo "</div>";

}

echo "</div> <!-- ROW END -->";
echo "</div> <!-- ITEM-LIST-CONT END -->";
}

?>

