<?php session_start(); ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<title><?php display_title(); ?></title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="Buy Gaming Laptops Online">
	<meta name="author" content="Dennis Allan Barba">
	<meta name="keywords" content="bluefire,gaming,laptops,capstone-2,project,bootcamp,tuitt">
	<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1">
	<link rel="shortcut icon" href="common/img/bluefire-favicon-16-32-48.ico">
	<link rel="stylesheet" href="assets/lib/normalize/normalize.css">
	<link rel="stylesheet" href="assets/lib/materialize/css/materialize.min.css">
	<link rel="stylesheet" href="assets/lib/materialize/css/materialize-social.css">
	<link rel="stylesheet" href="common/css/capstone2-style.css">
	<link href="https://cdn.rawgit.com/michalsnik/aos/2.1.1/dist/aos.css" rel="stylesheet">
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Libre+Barcode+39|Orbitron|Titillium+Web|Exo+2" rel="stylesheet">
	<script src="assets/lib/jquery/js/jquery-3.2.1.min.js"></script>
	<script src="https://cdn.rawgit.com/michalsnik/aos/2.1.1/dist/aos.js"></script>
	<script defer src="assets/lib/materialize/js/materialize.min.js"></script>
	<script defer src="common/js/capstone2-script.js"></script>
	<script src="https://use.fontawesome.com/3355809511.js"></script>
</head>
<div class="page_container">
<body>
		<div id="preloader">
			<div class="preloader_logo_cont valign-wrapper">
				<img class="preloader_logo center" id="bluefire_logo" src="common/img/bluefire-logo-full-gray-anim.svg" alt="BlueFire Logo">
			</div>
		</div>
		<?php require "partials/header.php"; ?>
		<main>
			<div id="header_buffer"></div>
			<?php display_content(); ?>
			<div id="footer_buffer"></div>
		</main>
		<aside>
			<?php require "user_menu.php"; ?>
		</aside>
		<?php require "partials/footer.php"; ?>
</body>
</div>
</html>