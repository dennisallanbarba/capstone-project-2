$(document).ready(function(){

     // Init AOS
     AOS.init();

     // Init Carousel Slider
     $('.carousel.carousel-slider').carousel({
          fullWidth:true,
          dist:1000,
     });

     setTimeout(hero_autoplay, 9000);
     function hero_autoplay() {
          setTimeout(hero_autoplay, 9000);
          $('.carousel.carousel-slider').carousel('next');
     }
     
     // INITS
     // Init Slider
     $('.slider').slider();

     // Init Modal
     $('.modal').modal();

     // Init Sidenav
     $('.button-collapse').sideNav();

     // Init Dropdown
     $('.dropdown-button').dropdown();

     // Init Select Element
     $('select').material_select();

     $('.pushpin_nav').pushpin({
          top: 0,
          bottom: 1000000,
          offset: 0
     });

     $('.pushpin_nav1').pushpin({
          top: 25,
          bottom: 1000000,
          offset: 0
     });

     $('.pushpin_nav2').pushpin({
          top: 85,
          bottom: 1000000,
          offset: 60
     });


     $('#close_side_nav').click(function(){
          $('.button-collapse').sideNav('hide');
     });


     $('.parallax').parallax();

     // MODALS
     $('a[href="#modal_login"]').click(function(){
          $('.button-collapse').sideNav('hide');
     });

     $('a[href="#modal_register"]').click(function(){
          $('.button-collapse').sideNav('hide');
     });

     $('#login_to_reg_quicklink').click(function(){
          $('#modal_login').modal('close');
     });

     $('#reg_to_login_quicklink').click(function(){
          $('#modal_register').modal('close');
     });
     // END MODALS


     // REGISTRATION
     var pwCheck = 0;
     var idCheck = 0;
     var emailCheck = 0;

     // PASSWORD MATCH AND LENGTH CHECK
     $('#user_pw_reg, #user_pwc_reg').on("input", function() {
          var pw1 = $('#user_pw_reg').val();
          var pw2 = $('#user_pwc_reg').val();
          if (pw1.length > 0 && pw2.length > 0) { /* FIRST, CHECK TO SEE IF BOTH PW FIELD HAVE VALUES */
               if (pw1.length < 8 || pw2.length < 8) {
                    $('#password_warning span').html(' Password must be at least 8 characters long.');
                    $('#password_warning i').html('info_outline');
                    $('#password_warning').css('color', 'red');
                    pwCheck = 0;
               } else if (pw1 != pw2 ) { /* SECOND, CHECK TO SEE IF BOTH PW FIELD ARE EQUAL */
                    $('#password_warning span').html(' Passwords do not match');
                    $('#password_warning i').html('info_outline');
                    $('#password_warning').css('color', 'red');
                    pwCheck = 0;
               } else { /* ELSE IF PW FIELD NOT EQUAL, REJECT */
                    $('#password_warning span').html(' Passwords match');
                    $('#password_warning i').html('done_all');
                    $('#password_warning').css('color', 'lime');
                    pwCheck = 1;
               }
          }
          else { /* ELSE, IF ONE FIELD DON'T HAVE LENGTH, NO WARNING*/
               $('#password_warning span').html('');
               $('#password_warning i').html('');
               pwCheck = 0;
          };
          regChecker();
     });
     // END PASSWORD MATCH AND LENGTH CHECK

     // USER ID CHECK AVAILABILITY 
     $('#user_id_reg').on('input', function(){
          var userID = $('#user_id_reg').val();
          $.ajax({
              method: 'post',
              url: 'registercheck.php',
              data: {
               userID: userID
          },
          success: function(data){
               if (userID.length < 5) {
                    $('#id_warning i').html('info_outline');
                    $('#id_warning span').html(' User ID should at least be 5 characters in length');
                    $('#id_warning').css('color', 'gray');
                    idCheck = 0;
                    regChecker();
               } else if(data=='taken'){
                    $('#id_warning i').html('info_outline');
                    $('#id_warning span').html(' This user ID already in use.');
                    $('#id_warning').css('color', 'red');
                    idCheck = 0;
                    regChecker();
               } else if(data=='available') {
                    $('#id_warning i').html('done');
                    $('#id_warning span').html(' This user ID is available.');
                    $('#id_warning').css('color', 'lime');
                    idCheck = 1;
                    regChecker();
               }
          }
     })
     });

     // EMAIl CHECK AVAILABILITY 
     $('#user_email_reg').on('input', function(){
          var userEmail = $('#user_email_reg').val();
          $.ajax({
              method: 'post',
              url: 'registercheck.php',
              data: {
               userEmail: userEmail
          },
          success: function(data){
               if (userEmail.length < 5) {
                    $('#email_warning i').html('info_outline');
                    $('#email_warning span').html(' Please enter a valid e-mail.');
                    $('#email_warning').css('color', 'gray');
                    emailCheck = 0;
                    regChecker();
               } else if(data=='taken'){
                    $('#email_warning i').html('info_outline');
                    $('#email_warning span').html(' This e-mail is already registered to another user.');
                    $('#email_warning').css('color', 'red');
                    emailCheck = 0;
                    regChecker();
               } else if(data=='available') {
                    $('#email_warning i').html('done');
                    $('#email_warning span').html(' E-mail is unregistered.');
                    $('#email_warning').css('color', 'lime');
                    emailCheck = 1;
                    regChecker();
               }
          }
     })
     });

     // ALL REGISTRATION REQUIREMENTS CHECKER
     function regChecker() {
          if(pwCheck == 1 && idCheck == 1 && emailCheck == 1) {
               $('#submit_reg').prop('disabled', false);
               $('#submit_reg').submit(function(){
                    return true;
               })
          } else {
               $('#submit_reg').prop('disabled', true);
               $('#submit_reg').submit(function(){
                    return false;
               })
          }
     }
     regChecker();
     // END REGISTRATION REQUIREMENTS CHECKER


     $('#logout_btn').click(function() {
          location.href = "logout.php";     
     });

     $('.shop_link').click(function() {
          location.href = "shop.php";     
     });


     // Timer logout page
     setTimeout(countdown, 1000);
     function countdown(){
          var x = $('.cdtimer').html();
          if (x > 0) {
               x--;
               setTimeout(countdown, 1000);
               $('.cdtimer').html(x);
          }
     }

     // CUSTOM BACK TO TOP SMOOTH SCROLL
     $("#back_to_top").hide();
     $(function () {
          $(window).scroll(function () {
               if ($(this).scrollTop() > 100) {
                    $('#back_to_top').fadeIn();
               } else {
                    $('#back_to_top').fadeOut();
               }
          });

          $('#back_to_top').click(function () {
               $('body,html').animate({scrollTop: 0}, 1000);
               return false;
          });
     });

     // BLUEFIRE QUICK MENU
     $('.pulsehover').mouseenter(function(){
          $(this).addClass('pulse');
     });

     $('.pulsehover').mouseleave(function(){
          $(this).removeClass('pulse');
     });

     // ADMIN ADD NEW PRODUCT BUTTON
     // $('#add_item_button').click(function(){
     //      $.ajax({
     //           method: post,
     //           url: 
     //      })
     // });


});


// PRELOADER - I PUT THIS OUTSIDE I FOUND IT CAN BE BUGGY IF INSIDE READY FUNCTION
$(window).on("load",function(){
     $('#preloader').fadeOut('slow',function(){
          $(this).remove();
     });
});
