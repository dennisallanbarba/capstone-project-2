  <div class="fixed-action-btn horizontal">
    <div class="cart-count-cont valign-wrapper tooltipped" data-tooltip="0 items on cart" data-position="top">
      <span id="cart_count center">0</span>
    </div>
    <a class="btn-floating btn-large blue accent-4 pulsehover" id="bluefire_quick_menu">
      <i class="large material-icons">shopping_cart</i>
    </a>
    <ul>
      <li><a id="back_to_top" class="btn-floating blue tooltipped light-blue darken-4 pulsehover" data-tooltip="Back to top" data-position="top"><i class="material-icons">vertical_align_top</i></a></li>
      <li><a href="home.php" class="btn-floating red accent-4 tooltipped pulsehover" data-tooltip="BlueFire Home" data-position="top"><i class="material-icons">home</i></a></li>
      <li><a href="shop.php" class="btn-floating blue accent-4 tooltipped pulsehover" data-tooltip="BlueFire Shop" data-position="top"><i class="material-icons">store</i></a></li>
      <li><a class="btn-floating green darken-3 tooltipped pulsehover" data-tooltip="View Cart" data-position="top"><i class="material-icons">shopping_cart</i></a></li>
    </ul>
  </div>
        