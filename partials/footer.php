<footer class="page-footer grey darken-4">
	<div class="container">
		<div class="row">

			<div class="col s12 m6 l4">
				<h5>BlueFire Philippines</h5>
				<hr>
				<p class="footer_p_desc">BlueFire is an online site dealing in cutting-edge, high-performance devices and computer accessories. Catering to the needs of E-Sports enthusiasts, video game hobbyists and several businesses in the Philippines, BlueFire was initially developed by Dennis Allan Barba for his 2nd capstone project during his stay at the awesome Tuitt Coding Bootcamp.</p>
			</div>

			<div class="col s12 m6 l4">
				<h5>Cutting-Edge Gear</h5>
				<hr>
				<p class="footer_p_desc">Video game fanatics are constantly looking for the newest and most powerful gear to dominate their favorite games. BlueFire answers this need by providing its customers only the latest and most powerful devices in the market.</p>
			</div>

			<div class="col s12 m6 l4">
				<h5>Quick Links</h5>
				<hr>
				<ul>
					<li><a href="#!">Customer Service</a></li>
					<li><a href="#!">Terms & Conditions</a></li>
					<li><a href="#!">About Us</a></li>
				</ul>
			</div>
		</div>
	</div>

	<div class="container">
		<div class="row">
			<div class="col s12 m6 l4">	
				<h5>Payment Methods</h5>
				<hr>
				<img class="extra_pay_icon" src="common/img/bdo.jpg">
				<img class="extra_pay_icon" src="common/img/cebuana.jpg">
				<i class="fa fa-cc-paypal fa-3x" aria-hidden="true"></i>
				<i class="fa fa-cc-mastercard fa-3x" aria-hidden="true"></i>
				<i class="fa fa-cc-visa fa-3x" aria-hidden="true"></i>
				<i class="fa fa-cc-amex fa-3x" aria-hidden="true"></i>
				<i class="fa fa-cc-discover fa-3x" aria-hidden="true"></i>
			</div>
			<div class="col s12 m6 l4">	
				<h5>Verified Secure</h5>
				<hr>
				<div class="certification_container center"><img class="iso_img" src="common/img/iso-27001.png" alt="ISO Certified"></div>
			</div>
			<div class="social_icon_group col s12 m6 l4">	
		  		<h5>Follow BlueFire</h5>
		  		<hr>
		  		<a href="#!"><i class="fa fa-facebook-square fa-2x" aria-hidden="true"></i></a>
		  		<a href="#!"><i class="fa fa-twitter-square fa-2x" aria-hidden="true"></i></a>
		  		<a href="#!"><i class="fa fa-tumblr-square fa-2x" aria-hidden="true"></i></a>
		  		<a href="#!"><i class="fa fa-instagram fa-2x" aria-hidden="true"></i></a>
		  		<a href="#!"><i class="fa fa-youtube-square fa-2x" aria-hidden="true"></i></a>
		  		<a href="#!"><i class="fa fa-pinterest-square fa-2x" aria-hidden="true"></i></a>
		  		<a href="#!"><i class="fa fa-google-plus-square fa-2x" aria-hidden="true"></i></a>
			</div>
			<div class="col s12 m6 l4">
				<h5>Download the BlueFire App</h5>
				<hr>
				<a class="footer_app_icon waves-effect waves-bluefire" href="#!"><img class="app_link_icon responsive-img" src="common/img/gplay-link-image.png" alt="Get the App on Google Play"></a>
				<a class="footer_app_icon waves-effect waves-bluefire" href="#!"><img class="app_link_icon responsive-img" src="common/img/appstore-link-img.png" alt="Get the App on the Apple App Store"></a>
			</div>
		</div>
	</div>

	<div class="footer-copyright">
		<div class="container center">
			<hr style="border-color: gray">
			Dennis Allan Barba © 2018
			<br>
			<p id="disclaimer_notice"><strong>Disclaimer: </strong>No copyright infringement is intended. This website is purely for educational purposes and not for profit. This website contains copyrighted material and assets, the use of which has not always been specifically authorized by the copyright owner. Some assets used are not owned by the developer and unless otherwise stated, the credit goes to their owners.</p>
		</div>
	</div>
</footer>