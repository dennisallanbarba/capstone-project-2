<header>
<nav class="nav-extended">
	<!-- LEVEL 1 NAV -->
	<div id="nav_upper_bar" class="nav-wrapper light-blue darken-4 hide-on-med-and-down">
		<ul class="left typ-nunito upper-greet">
			<li class="typ-orbitron">WELCOME TO BLUEFIRE, <?php echo isset($_SESSION['userID']) ? strtoupper(($_SESSION['userID'])) : 'GUEST' ?>!</li>
		</ul>
		<ul class="right">
			<li><a href="#!" class="waves-effect waves-light"><i class="material-icons tiny left">phone</i>CUSTOMER SERVICE</a></li>
			<li><a href="#1" class="waves-effect waves-light"><i class="material-icons tiny left">history</i>TRACK ORDERS</a></li>

			<!-- CHECK TO SEE IF A USER IS LOGGED IN, OTHERWISE RENDER THE LOGIN AND REGISTER BUTTONS -->
			<?php echo isset($_SESSION['userID']) ?
			'<li><button class="btn waves-effect waves-light light-blue darken-3 modal-trigger"><i class="material-icons left">person</i>MY ACCOUNT [' . $_SESSION['userID'] . ']</button></li>
			<li><button id="logout_btn" class="btn waves-effect waves-light red darken-4"><i class="material-icons left">launch</i>LOGOUT</button></li>'
			:
			'<li><button class="btn waves-effect waves-light light-blue darken-3 modal-trigger" href="#modal_login"><i class="material-icons left">exit_to_app</i>LOGIN</button></li>
			<li><button class="btn waves-effect waves-light blue darken-4 modal-trigger" href="#modal_register"><i class="material-icons left">person_add</i>REGISTER</button></li>'
			?>

		</ul>
	</div>
	<!-- END LEVEL 1 NAV -->

	<!-- LEVEL 2 NAV -->
	<div id="nav_lower_bar" class="pushpin_nav1 nav-wrapper grey darken-4">
		<a href="#" onclick="Materialize.showStaggeredList('#mobile_main_menu')" data-activates="mobile_main_menu" class="button-collapse"><i class="material-icons">menu</i></a>
		<a href="home.php" class="brand-logo center">
			<img id="bluefire_logo" src="common/img/bluefire-logo-full-gray-anim.svg" alt="BlueFire Logo">
		</a>
		<form class="search-wrapper right hide-on-small-only">
	    <input id="search_input" placeholder="Search BlueFire">
	    <a id="search_button">
				<i class="material-icons right">search</i>
			</a>
    </form>
    <div class="lower_bar_strap hide-on-med-and-down">&nbsp&nbsp&nbsp&nbsp&nbspCUSTOM</div>
    <div class="lower_bar_strap hide-on-med-and-down">&nbsp&nbsp&nbsp&nbsp&nbspCAPSTONE-2</div>
    <div class="lower_bar_strap hide-on-med-and-down">&nbsp&nbsp&nbsp&nbsp&nbspPROJECT</div>
  </div>
  <!-- END LEVEL 2 NAV -->

	<!-- LEVEL 3 NAV - CHANGED MY MIND, NO MORE BREADCRUMBS -->
	<div id="nav_bread_crumbs" class="pushpin_nav2 nav-wrapper z-depth-1 hide-on-med-and-down">
		<ul class="left">
			<li><a href="home.php" class="waves-effect waves-bluefire"><i class="material-icons left">home</i>HOME</a></span></li>
			<li><a href="shop.php" class="waves-effect waves-bluefire"><i class="material-icons left">store</i>SHOP</a></span></li>
			<li><a href="#!" class="waves-effect waves-bluefire"><i class="material-icons left">star</i>BEST DEALS</a></span></li>
		</ul>
		<ul class="right">
			<li><a href="#!" class="waves-effect waves-bluefire"><i class="material-icons left">library_books</i>TERMS AND CONDITIONS</a></span></li>
			<li><a href="#!" class="waves-effect waves-bluefire"><i class="material-icons left">help_outline</i>ABOUT US</a></span></li>
		</ul>
	</div>
</nav>
<!-- END LEVEL 3 NAV -->

  <!-- MOBILE MAIN MENU UL -->
  <ul class="side-nav black left" id="mobile_main_menu">
  	<div><button class="btn black right waves-effect waves-teal" id="close_side_nav"><i class="material-icons right">keyboard_return</i></button></div>
  	<br>
  	<div class="logo_mobile_container center"><img id="logo_mobile_menu" src="common/img/bluefire-logo-full-gray-anim.svg"></div>
  	<?php echo isset($_SESSION['userID']) ?
  	"<li><a class='waves-effect waves-bluefire' href='#!n'><i class='material-icons left'>person</i>MY ACCOUNT [" . strtoupper($_SESSION['userID']) . "]</a></li>"
  	:
  	"<li><a class='waves-effect waves-bluefire modal-trigger' href='#modal_login'><i class='material-icons left'>exit_to_app</i>LOGIN</a></li>
  	<li><a class='waves-effect waves-bluefire modal-trigger' href='#modal_register'><i class='material-icons left'>person_add</i>REGISTER</a></li>"
  	?>
  	<li><a class="waves-effect waves-bluefire" href="home.php"><i class="material-icons left">home</i>HOME</a></li>
  	<li><a class="waves-effect waves-bluefire" href="shop.php"><i class="material-icons left">store</i>SHOP</a></li>
  	<li><a class="waves-effect waves-bluefire" href="#"><i class="material-icons left">star</i>BEST DEALS</a></li>
  	<li><a class="waves-effect waves-bluefire" href="#"><i class="material-icons left">history</i>TRACK ORDERS</a></li>
  	<li><a class="waves-effect waves-bluefire" href="#"><i class="material-icons left">library_books</i>TERMS AND CONDITIONS</a></li>
  	<li><a class="waves-effect waves-bluefire" href="#"><i class="material-icons left">help_outline</i>ABOUT US</a></li>
  	<?php echo isset($_SESSION['userID']) ?
  	'<li><a class="waves-effect waves-bluefire" href="logout.php"><i class="material-icons left">launch</i>LOGOUT</a></li>'
  	: ''
  	?>
  	<div id="mobile_app_section">
  		<a class="waves-effect waves-bluefire" href="#!"><img class="app_link_icon responsive-img" src="common/img/gplay-link-image.png" alt="Get the App on Google Play"></a>
  		<a class="waves-effect waves-bluefire" href="#!"><img class="app_link_icon responsive-img" src="common/img/appstore-link-img.png" alt="Get the App on the Apple App Store"></a>
  		<div class="grey-text">Dennis Allan Barba © 2018</div>
  	</div>
  </ul>
  <!-- END MOBILE MAIN MENU UL -->

<!-- HEADER MODALS -->
<!-- LOGIN MODAL -->
<div id="modal_login" class="modal grey darken-4">

	<div id="login_h4" class="row center modal-content col s12 white-text typ-orbitron"  style="">
			<h4>LOGIN</h4>
	</div>

<div class="login_cont">
	<form class="row s12" id="login_form" action="authenticate.php" method="POST">
		<div class="center white-text col s12 m-bottom-15">Login to BlueFire with your registered username and password.</div>

		<div class="input-field col s12 center white-text">
			<i class="material-icons prefix">account_circle</i>
			<input type="text" id="user_id_login" name="userID" class="validate" required>
			<label for="user_email_input">Username</label>
		</div>

		<div class="input-field col s12 white-text">
			<i class="material-icons prefix">vpn_key</i>
			<input type="password" id="user_pw_login" name="userPassword" required>
			<label for="user_email_input">Password</label>
		</div>

		<div class="row center col s12">
			<button type="submit" value="login" class="waves-effect waves-bluefire btn social center blue accent-4">
			<i class="bluefire_ico"></i>&nbsp&nbspSIGN IN TO BLUEFIRE</button>
			<div class="row center m-top-15">
		      <input type="checkbox" id="remember_tick" />
		      <label for="remember_tick"><strong class="white-text">Remember me</strong></label>
			</div>
		</div>

	</form>

	<h6 class="center white-text row col s12">Don't have a BlueFire account yet?<br><br><a id="login_to_reg_quicklink" class="modal-trigger" href="#modal_register">REGISTER NOW</a> to get access to our awesome deals!</h6>
	<div class="row center">
		<span class="white-text typ-orbitron">OR</span>
	</div>
	<div class="row center col s12">
		<a class="waves-effect waves-light btn social facebook center">
		<i class="fa fa-facebook"></i> Sign in with facebook</a>
	</div>
	<div class="row center col s12">
		<a class="waves-effect waves-light btn social google center">
		<i class="fa fa-google"></i> Sign in with google</a>
	</div>
</div>
</div>
<!-- END MODAL LOGIN -->

<!-- REGISTER MODAL -->
<div id="modal_register" class="modal grey darken-4">

	<div id="login_h4" class="row center modal-content col s12 white-text typ-orbitron"  style="">
			<h4>REGISTRATION</h4>
	</div>

	<div class="reg_cont">
		<form class="row s12" id="reg_form" action="register_endpoint.php" method="POST">

			<div class="center white-text col s12 m-top-15 m-bottom-15">Fill out the form below to register a new BlueFire Account.</div>

			<!-- FIRST NAME REG -->
			<div class="input-field col s12 m12 l6 center white-text">
				<i class="material-icons prefix">perm_identity</i>
				<input type="text" id="user_firstname_reg" name="userFirstName" class="validate" required>
				<label for="user_firstname_reg">First Name</label>
			</div>

			<!-- LAST NAME REG -->
			<div class="input-field col s12 m12 l6 center white-text">
				<i class="material-icons prefix">perm_identity</i>
				<input type="text" id="user_lastname_reg" name="userLastName" class="validate" required>
				<label for="user_lastname_reg">Last Name</label>
			</div>

			<!-- EMAIL ADDRESS REG -->
			<div class="input-field col s12 m12 l6 center white-text">
				<i class="material-icons prefix">email</i>
				<input type="email" id="user_email_reg" name="userEmail" class="validate" required>
				<label for="user_email_reg">Email Address</label>
				<div id="email_warning"><i class="material-icons tiny"></i><span></span></div>
			</div>

			<!-- MOBILE PHONE NUMBER REG -->
			<div class="input-field col s12  m12 l6 center white-text">
				<i class="material-icons prefix">settings_cell</i>
				<input type="number" id="user_mobile_reg" name="userMobile" class="validate" required>
				<label for="user_mobile_reg">Mobile Phone #</label>
			</div>

			<!-- CHOSEN USER NAME/ID REG -->
			<div class="input-field col s12 center white-text">
				<i class="material-icons prefix">account_circle</i>
				<input type="text" id="user_id_reg" name="userID" class="validate" required>
				<label for="user_id_reg">BlueFire ID</label>
				<div id="id_warning"><i class="material-icons tiny"></i><span></span></div>
			</div>

			<!-- PASSWORD REG -->
			<div class="input-field col s12 white-text">
				<i class="material-icons prefix">vpn_key</i>
				<input type="password" id="user_pw_reg" name="userPassword" required>
				<label for="user_pw_reg">Enter a Password</label>
			</div>

			<!-- CONFIRM PASSWORD REG -->
			<div class="input-field col s12 white-text">
				<i class="material-icons prefix">vpn_key</i>
				<input type="password" id="user_pwc_reg" required>
				<label for="user_pwc_reg">Confirm Password</label>
				<div  id="password_warning" class="center"><i class="material-icons tiny"></i><span></span></div>
			</div>

			<!-- SUBMIT REGISTRATION REG -->
			<div class="row center col s12 m-top-15">
				<button type="reset" id="reset_reg" class="waves-effect waves-red btn grey blue darken-3">
				<i class="material-icons left tiny">clear_all</i> RESET FIELDS</button>
			</div>

			<!-- REGISTRATION ACCEPT TERMS -->
			<div class="row center col s12 m-top-15">
				<input type="checkbox" id="accept_terms_tick" required />
	      <label for="accept_terms_tick"><strong class="white-text"> I accept BlueFire's <a href="#!">Terms and Conditions</a></strong></label>
			</div>

			<!-- SUBMIT REGISTRATION REG -->
			<div class="row center col s12 m-top-15">
				<button disabled type="submit" id="submit_reg" class="waves-effect waves-bluefire btn social center blue accent-4">
				<i class="bluefire_ico"></i>&nbsp&nbspREGISTER ACCOUNT</button>
			</div>

			<!-- ALREADY HAVE AN ACCOUNT REG -->
			<h6 class="center white-text row col s12">Already have a BlueFire account?<br><br>You may sign-in <a id="reg_to_login_quicklink" class="modal-trigger" href="#modal_login">HERE</a>.</h6>

		</form>
	</div>
</div>
<!-- END MODAL REGISTER -->

</header>

